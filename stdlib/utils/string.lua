--- Extends Lua 5.2 string.
-- @module string
-- @see string

-- trimmed-down version for only string.split

--- Splits a string into an array.
-- *Note:* Empty split substrings are not included in the resulting table.
-- <p>For example, `string.split("foo.bar...", ".", false)` results in the table `{"foo", "bar"}`.
-- @tparam string s the string to split
-- @tparam[opt="."] string sep the separator to use.
-- @tparam[opt=false] boolean pattern whether to interpret the separator as a lua pattern or plaintext for the string split
-- @treturn {string,...} an array of strings
function string.split(s, sep, pattern)
    sep = sep or '.'
    sep = sep ~= '' and sep or '.'
    sep = not pattern and sep:gsub('([^%w])', '%%%1') or sep
    

    local fields = {}
    local start_idx, end_idx = s:find(sep)
    local last_find = 1
    while start_idx do
        local substr = s:sub(last_find, start_idx - 1)
        if substr:len() > 0 then
            table.insert(fields, s:sub(last_find, start_idx - 1))
        end
        last_find = end_idx + 1
        start_idx, end_idx = s:find(sep, end_idx + 1)
    end
    local substr = s:sub(last_find)
    if substr:len() > 0 then
        table.insert(fields, s:sub(last_find))
    end
    return fields
end


